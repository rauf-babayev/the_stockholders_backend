class Model {
    constructor(mongodb) {
        this.mongodb = mongodb;
        this.leadsArray = [];
        this.adminsArray = [];

        this.init();
    }

    init = () => {
        this.mongodb.getAllDocs('leadsData', this.setLeadsArray.bind(this));
        this.mongodb.getAllDocs('adminsAcc', this.setAdminsArray.bind(this));
    }

    getLeads = () => {
        const array = [];
        this.leadsArray.forEach(item => {
            array.push(item.accountInfo);
        })

        return array;
    };

    getAdmins = () => this.adminsArray;

    insertLead = data => this.mongodb.insertData("leadsData", data);

    deleteLead = id => {
        this.mongodb.deleteLead("leadsData", id);
    }

    setLeadsArray = data => {
        this.leadsArray = data;
        console.log('The leads array have set');
    }

    setAdminsArray = data => {
        this.adminsArray = data;
        console.log('The admins array have set')
    }

    updateLeadProfile = data => {
        const { oldOne, newOne } = data;
        this.mongodb.updateLead("leadsData", oldOne, newOne);
    }

    findAccountInArray = (array, accountData) => array.find(item => JSON.stringify(item.accountInfo.account) === JSON.stringify(accountData))
}

module.exports = Model;