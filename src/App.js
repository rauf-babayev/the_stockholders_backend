const express = require('express');
const path = require('path');
const { response } = require('express');

class App{
    constructor(controller) {
        this.app = express();
        this.app.use(express.json());
        this.app.use('/', express.static(path.resolve(__dirname, '../public')));
        this.controller = controller;
        this.init()
    }

    init = () => {
        this.app.get('/checkLeadAuthorize',this.checkLeadAuthorize);
        this.app.get('/checkAdminAuthorize',this.checkAdminAuthorize);
        this.app.get('/getLeadsList',this.getLeadsList);
        this.app.put('/addNewLead',this.addNewLead);
        this.app.put('/updateLeadsInfo',this.updateLeadsInfo);
        this.app.delete('/deleteLeads',this.deleteLead);
    }

    getApp = () => this.app;

    addNewLead = (req, res) => {
        const { data } = req;
        this.controller.addNewLeadObj(data);

        res.end();
    }

    deleteLead = (req, res) => {
        const { body } = req;
        this.controller.deleteLeadById(body);

        res.end()
    }

    getLeadsList = (req, res) => {
        const data = this.controller.returnLeadsList();

        res.json(data);
        res.end();
    }

    updateLeadsInfo = (req, res) => {
        const { body } = req;
        this.controller.updateLeadById(body);

        res.end();
    }

    checkLeadAuthorize = (req, res) => {
        const { body } = req;
        const authResult = this.controller.checkLeadAccountExist(body);

        res.json(authResult);
        res.end();
    }

    checkAdminAuthorize = (req, res) => {
        const { body } = req;
        const authResult = this.controller.checkAdminAuthorize(body);

        res.json(authResult);
        res.end();
    }
}

module.exports = App;