// how to connect to our server

/checkLeadAuthorize
gets the object {login, password} and checks for existence
    if the account exists then return its profile
    else return false

/checkAdminAuthorize
gets the object {login, password} and checks for existence
    if the account exists then return its profile
    else return false

/getLeadsList
[{}, {}] returns leads array

/addNewLead
gets the object with lead's info which should be pushed into leads array

/deleteLeads
gets the object with leads info in backend must be deleted from leads array

/updateLeadsInfo
gets the object with updated lead's info, finds the lead in leads array and updates
