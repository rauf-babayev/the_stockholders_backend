const { MongoClient } = require('mongodb');

class MongoDB {
    constructor() {
        this._url = "mongodb+srv://rauf00:password00@cluster0-t2r3i.mongodb.net/leads_base?retryWrites=true&w=majority";
    }

    insertData = (collection, data) => {
        MongoClient.connect(this._url, function(err, db) {
            if (err) throw err;
            const dbo = db.db("leads_base");
            dbo.collection(collection).insertOne(data, function(err, res) {
              if (err) throw err;
              console.log("1 document inserted");
              db.close();
            });
        });
    };

    getAllDocs = (collection, cb) => {
         MongoClient.connect(this._url, function(err, db) {
            if (err) throw err;
            const dbo = db.db("leads_base");
            dbo.collection(collection).find({}).toArray(function(err, result) {
                if (err) throw err;
                cb(result)
                db.close();
            });
        });
    }

    deleteLead = (collection, data) => {
         MongoClient.connect(this._url, function(err, db) {
            if (err) throw err;
            const dbo = db.db("leads_base");
            dbo.collection(collection).deleteOne(data, function(err, result) {
                if (err) throw err;
                console.log('Lead deleted');
                db.close();
            });
        });
    }

    updateLead = (collection, data, updatedData) => {
         MongoClient.connect(this._url, function(err, db) {
            if (err) throw err;
            const dbo = db.db("leads_base");
            dbo.collection(collection).updateOne(data, {  $set: updatedData }, function(err, result) {
                if (err) throw err;
                console.log('Lead updated');
                db.close();
            });
        });
    }
}

module.exports = MongoDB;
