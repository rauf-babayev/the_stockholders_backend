const App = require('./App');
const http = require('http');
const Model = require('./Model');
const MongoDB = require('./MongoDB');
const Controller = require('./Controller');

class Server {
    constructor(port) {
        this.port = port;
        this.mongodb = new MongoDB();
        this.model = new Model(this.mongodb)
        this.controller = new Controller(this.model);
        this.app = new App(this.controller);
        this.server = http.createServer(null, this.app.getApp());
    }
    
    start = () => {
        console.log(`Server is running ${this.port}...`);
        this.server.listen(this.port);
    }
}

module.exports = Server;